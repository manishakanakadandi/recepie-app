import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ShoppingListComponent } from './header/shopping-list/shopping-list.component';
import { ShoppingListEditComponent } from './header/shopping-list/shopping-list-edit/shopping-list-edit.component';
import { RecepieListComponent } from './header/recepie-list/recepie-list.component';
import { RecepiesComponent } from './header/recepies/recepies.component';
import { RecepieDetailComponent } from './header/recepies/recepie-detail/recepie-detail.component';
import { RecepieItemComponent } from './header/recepie-item/recepie-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShoppingListComponent,
    ShoppingListEditComponent,
    RecepieListComponent,
    RecepiesComponent,
    RecepieDetailComponent,
    RecepieItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
