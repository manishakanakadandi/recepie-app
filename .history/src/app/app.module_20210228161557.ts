import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { ShoppingListComponent } from "./header/shopping-list/shopping-list.component";
import { ShoppingListEditComponent } from "./header/shopping-list/shopping-list-edit/shopping-list-edit.component";
import { RecepieListComponent } from "./recepies/recepie-list/recepie-list.component";
import { RecepiesComponent } from "./recepies/recepies.component";
import { RecepieDetailComponent } from "./recepies/recepie-detail/recepie-detail.component";
import { RecepieItemComponent } from "./recepies/recepie-list/recepie-item/recepie-item.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShoppingListComponent,
    ShoppingListEditComponent,
    RecepieListComponent,
    RecepiesComponent,
    RecepieDetailComponent,
    RecepieItemComponent,
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
