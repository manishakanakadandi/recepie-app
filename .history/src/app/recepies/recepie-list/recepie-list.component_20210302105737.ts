import { Component, OnInit } from "@angular/core";
import { Recepie } from "../recepie.model";
@Component({
  selector: "app-recepie-list",
  templateUrl: "./recepie-list.component.html",
  styleUrls: ["./recepie-list.component.css"],
})
export class RecepieListComponent implements OnInit {
  recepies: Recepie[] = [
    new Recepie(
      "chole bature",
      "A North Indian Dish",
      "https://i.pinimg.com/originals/8d/c5/31/8dc531fd1168a51c9e5bc9aca0d45051.jpg"
    ),
  ];
  constructor() {}

  ngOnInit(): void {}
}
